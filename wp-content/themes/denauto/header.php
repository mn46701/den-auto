<?php wp_head(); ?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= wp_title(); ?></title>
</head>
<body>


<div class="container-fluid">

    <div class="container">
        <header class="col-xs-12">

            <div class="menu col-xs-12">
                <a href="/">
                    Main
                </a>
                <a href="" class="prevent_default">
                    Пасажири
                    <ul>
                        <li>
                            <a href="<?= get_permalink(get_page_by_path('dogovir-ofertu')); ?>">
                                Договір оферти
                            </a>
                        </li>
                        <li>
                            <a href="<?= get_permalink(get_page_by_path('rules-of-transportation')); ?>">
                                Правила перевезень
                            </a>
                        </li>
                        <li>
                            <a href="<?= get_permalink(get_page_by_path('return-tickets')); ?>">
                                Повернення квитків
                            </a>
                        </li>
                    </ul>
                </a>
                <a href="<?= get_permalink(get_page_by_path('shares')); ?>">
                    Акції
                </a>
                <a href="<?= get_permalink(get_page_by_path('faq')); ?>">
                    FAQ
                </a>
                <a href="<?= get_permalink(get_page_by_path('about-us')); ?>">
                    Про нас
                </a>
            </div>

        </header>
    </div>

</div>

<div class="container">