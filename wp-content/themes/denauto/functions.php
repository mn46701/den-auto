<?php
add_theme_support( 'post-thumbnails' );
function register_my_menu() {
    register_nav_menu('main-menu',__( 'Main Menu' ));
}
add_action( 'init', 'register_my_menu' );

function my_scripts() {
    wp_enqueue_script( 'jquery', '//ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js' );
    wp_enqueue_script( 'main-script', get_template_directory_uri() . '/js/main.js' );
    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css' );
    wp_enqueue_style( 'main-style', get_template_directory_uri() . '/css/main.css' );
}
add_action( 'wp_enqueue_scripts', 'my_scripts' );