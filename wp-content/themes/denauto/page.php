<?php get_header(); ?>
<?php the_post(); ?>
<div class="container">
    <h1><?= get_the_title(); ?></h1>
    <div>
        <?= get_the_content(); ?>
    </div>
</div>
<?php get_footer(); ?>
